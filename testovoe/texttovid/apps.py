from django.apps import AppConfig


class TexttovidConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'texttovid'
