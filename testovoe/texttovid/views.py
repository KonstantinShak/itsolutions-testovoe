import os
from django.http import HttpResponseRedirect, FileResponse
from django.views.generic import ListView
from django.views.generic.base import reverse
from .models import TextHistory
from .services.Converter import create_video_from_text


def download_file(request, text:str):
    file_path = f'./texttovid/services/vids/{text}.mp4'
    response = FileResponse(open(file_path, 'rb'))
    response['Content-Disposition'] = f'attachment; filename="{text}"'
    return response
    
def parse_text(text:str) -> None:
    create_video_from_text(text)
    TextHistory.objects.get_or_create(text=text)

def delete(request, id):
    some_line = TextHistory.objects.get(id=id)
    field_object = TextHistory._meta.get_field("text")
    field_value = field_object.value_from_object(some_line)
    os.remove(f"./texttovid/services/vids/{field_value}.mp4")
    os.remove(f"./texttovid/services/imgs/{field_value}.jpg")
    some_line.delete()
    return HttpResponseRedirect(reverse('main'))

class MainPage(ListView):
    model = TextHistory
    template_name = 'mainpage.html'

    def get(self, request, *args, **kwargs):
        text = request.GET.get('text', None)
        if text:
            parse_text(text)
            return HttpResponseRedirect(reverse('download', kwargs={'text':text}))
        return super().get(request, *args, **kwargs)



    


