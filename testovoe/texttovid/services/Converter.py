from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from moviepy.editor import VideoFileClip, ImageClip, CompositeVideoClip
from .variables import SPEED_MULTIPLIER, IMG_FPS, LENGTH_MULTIPLIER


def create_video(name:str) -> None:
    finalList = []
    play_speed = len(name) * SPEED_MULTIPLIER 
    backTemp = VideoFileClip('./texttovid/services/vids/samples/sample.mp4').without_audio()
    back = backTemp
    finalList.append(back)
    image = ImageClip(f'./texttovid/services/imgs/{name}.jpg', duration=3)
    image = image.set_position(lambda t: ((play_speed*(t + 0))*(-1), 0))
    image.fps = IMG_FPS
    finalList.append(image)
    videoTemp = CompositeVideoClip(finalList)
    videoTemp.write_videofile(f"./texttovid/services/vids/{name}.mp4")

def create_image(sp_text:str) -> None:
    img = Image.open('./texttovid/services/imgs/samples/sample.png')
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype('./texttovid/services/font/Roboto-Medium.ttf', 80)
    draw.text((0,0), sp_text, (122, 122, 122), font=font)
    length = LENGTH_MULTIPLIER * len(sp_text)
    area = (0, 0, length, 100)
    cropped_img = img.crop(area)
    cropped_img.save(f'./texttovid/services/imgs/{sp_text}.jpg')

def create_video_from_text(name:str) -> None:
    create_image(name)
    create_video(name)



