from django.urls import path
from .views import MainPage, delete, download_file


urlpatterns = [
        path('', MainPage.as_view(), name='main'),
        path('delete/<int:id>', delete, name='delete'),
        path('download/<str:text>', download_file, name='download'),
        ]
