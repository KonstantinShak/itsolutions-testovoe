from django.db import models


class TextHistory(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    text = models.CharField(unique=True, max_length=50)
